package main

import (
	"fmt"
	"zinx-learn/ziface"
	"zinx-learn/znet"
)

/*
 基于Zinx框架来开发的 服务器端应用程序
*/

//ping test 自定义路由
type PingRouter struct {
	znet.BaseRouter
}

type HelloZinxRouter struct {
	znet.BaseRouter
}

//Test Handle
func (br *PingRouter) Handle(request ziface.IRequest) {
	fmt.Println("Call Router Handle...")
	//读取客户端的数据，再回写ping
	fmt.Println("recv from client: msgId = ", request.GetMsgID(), ", data = ", string(request.GetData()))
	err := request.GetConnection().SendMsg(request.GetMsgID(), []byte("PingRouter"))
	if err != nil {
		fmt.Println(err)
	}
}

func (br *HelloZinxRouter) Handle(request ziface.IRequest) {
	fmt.Println("Call Router Handle...")
	//读取客户端的数据，再回写ping
	fmt.Println("recv from client: msgId = ", request.GetMsgID(), ", data = ", string(request.GetData()))
	err := request.GetConnection().SendMsg(request.GetMsgID(), []byte("HelloZinxRouter"))
	if err != nil {
		fmt.Println(err)
	}
}

//创建链接之后执行钩子函数
func DoConnetionBegin(conn ziface.IConnection) {
	fmt.Println("===> DoConnetionBegin is called")
	if err := conn.SendMsg(202, []byte("DoConnetion BEGIN")); err != nil {
		fmt.Println(err)
	}
}

//链接端口之前的需要执行的函数
func DoConnectionLost(conn ziface.IConnection) {
	fmt.Println("===> DoConnectionLost is Called ... ")
	fmt.Println("conn ID = ", conn.GetConnID(), " is Lost ")
}

func main() {
	//1.创建一个server句柄，使用Zinx的api
	s := znet.NewServer("[zinx V0.6]")

	//注册链接hook钩子函数
	s.SetOnConnStart(DoConnetionBegin)
	s.SetOnConnStop(DoConnectionLost)

	//2.给当前zinx添加一个自定义的router
	s.AddRouter(0, &PingRouter{})
	s.AddRouter(1, &HelloZinxRouter{})
	//3.启动server
	s.Serve()
}

//Test PreHandle
